document.addEventListener("DOMContentLoaded", function() {
    const burgerMenu = document.querySelector(".burger-menu");
    const dropdown = document.querySelector(".dropdown");

    burgerMenu.addEventListener("click", function(event) {
        dropdown.classList.toggle("visible");
        dropdown.classList.toggle("hidden");
        event.stopPropagation(); 
    });

    document.addEventListener("click", function(event) {
        if (!dropdown.contains(event.target) && 
            !burgerMenu.contains(event.target)) {
            dropdown.classList.add("hidden");
            dropdown.classList.remove("visible");
        }
    });
});
