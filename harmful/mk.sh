#!/bin/bash

TARGET_DIR="purelyf"
INCLUDE_DIR="static"
BLOGTABLE="templates/blog_table.html"
NAVBAR="$INCLUDE_DIR/navbar.html"
STYLE="$INCLUDE_DIR/style.css"
SCRIPT="$INCLUDE_DIR/script.js"

rm -rf "$TARGET_DIR"
mkdir -p "$TARGET_DIR/blog"
mkdir "$TARGET_DIR/harmful"
mkdir "$TARGET_DIR/contact"
mkdir "$TARGET_DIR/pgp"

cp "pgp/index.html" "$TARGET_DIR/pgp/index.html"
cp "$INCLUDE_DIR/404.html" "$TARGET_DIR/404.html"
cp "robots.txt" "$TARGET_DIR/"
cp "$INCLUDE_DIR/image.webp" "$TARGET_DIR/image.webp"
cp "$INCLUDE_DIR/favicon/"* "$TARGET_DIR/"
cp harmful/favicon/* "$TARGET_DIR/harmful/"

mkdir "$TARGET_DIR/$INCLUDE_DIR"
cp "$STYLE" "$TARGET_DIR/$STYLE"
cp "$SCRIPT" "$TARGET_DIR/$SCRIPT"

echo "<!-- generated using bash -->" > "$BLOGTABLE"
find blog/ -type f -name "*.md" | sort -r | while read -r file; do
    #extract the header stuff in the markdown files
    sed -n '/^---$/,/^---$/ { /^---$/d; p }' "$file" > .header_tmp

    # extract metadata
    TITLE=$(yq -r ".title" .header_tmp)
    URLTITLE=$(yq -r ".urltitle" .header_tmp)
    DATE=$(yq -r ".date" .header_tmp)
    DESCRIPTION=$(yq -r ".description" .header_tmp)

    # make blog entry
    mkdir "$TARGET_DIR/blog/$URLTITLE"

    pandoc "$file" \
            --template="templates/blogtemplate.html" \
            -o "$TARGET_DIR/blog/$URLTITLE/index.html"

    echo "Built '$file'"
    
    POSTITEM=$(cat << EOF
<div class="post">
    <h1><a href="$URLTITLE">$TITLE</a></h1>
    <time>$DATE</time>
    <p align="justify">$DESCRIPTION</p>
</div>
EOF
)
    echo "$POSTITEM" >> "$BLOGTABLE"
    rm .header_tmp
done

# make index.html
pandoc "index.md" \
        --template="templates/hometemplate.html" \
        --output="$TARGET_DIR/index.html"

# make harmful/index.html
pandoc "harmful/index.md" \
        --template="templates/harmfultemplate.html" \
        --output="$TARGET_DIR/harmful/index.html"


# make blog/index.html
pandoc -i "$INCLUDE_DIR/blogindex.md" \
        --template="templates/blogindex.html" \
        --output "$TARGET_DIR/blog/index.html"


# On harmful, have a quote of the day, place it using sed and rerun each day
# this thing is not BLAZINGLY EPICLY FAST
# Want the posts to be sorted correctly? Add them YYYY-MM-DD-title.md
# No this doesn't rearrange your dependencies so you can write in a retarded
# order
