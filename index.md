---
title: "Purely Fictional"
bartitle: "Purely Fictional"
barcomment: ""
metadescription: "Pelle Hänel's. blog on functional programming, mathematics and (UNIX) philosophy"
---

A blog about philosophy, mathematics, and programming, as well as all the other 
great "joys" this existence bestows upon us
