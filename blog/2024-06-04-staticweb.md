---
title: "Simple Static Sites With Pandoc"
author: "Fox Mulder"
date: "2024-06-04"
urltitle: "pandocstatic"
bartitle: "Purely Simple: "
barcomment: "Pandoc Site"
metadescription: "using pandoc and bash to create static websites"
description: >
    Pandoc is a Haskell library and document converter that can be easily used
    to create static websites, without the usual overhead and forced 
    "custimizability", other frameworks promise. Bash + Pandoc is all you need.
---

_this is a sample post, it's not interesting_


I sometimes (think) I have something to say, even though many would question 
whether I should. Hence, I wanted to create a website, a painful and ugly task 
I dreaded for years. The complexity, learning a framework (bloat), and the 
countless questionable tutorials about a subject I have absolutely zero 
interest in was enough to keep me from creating any website whatsoever.

Unfortunately for the world, I made it and successfully created this page, a 
place where I can share my unhinged thoughts in a well organized manner, so 
that you can let those very thoughts influence yours.


## The Problem

The web is a bloated mess. It sucks so incredibly hard, even zoomers are
put off by the modern web, without having any baseline to compare against. 
Once a way to present some slightly formatted text, has become an 
abomination of web apps, bloatcode, ads, banners, popups and other grim and evil 
bullshit.

## The Solution

There is no solution for the modern web at large, as there is no monetary 
incentive to make it suck less. Tracking, ads, notifications are what makes the
cash flow, so nothing will change. 
However, one can try to not cause more evil oneself, and thus abstain from using
tracking, cookies, ads, banners, javascript, etc.

This page was created to be simple static site, providing the essentials of 
acceptable typesetting and information display. It's not done yet and will 
evolve over time.

There are frameworks and static site generators that try to reduce the bloat
and overhead associated with modern web development, e.g. [HUGO](https://gohugo.io/)
I however wanted a working site quickly and not grapple with a static site 
generator, because I barely even knew HTML and CSS. I considered using Hakyll[^note]
But then, a friend of 
mine had a solution, or at least so he claimed. `3sg`, inspired by
[saait](https://codemadness.org/saait.html). Saait is extremely simple, easy to 
learn and not very bloated. However, Saait uses a flat layout, 
and I don't like the way the code looks like. Does _codemadness_ hate 
recursion? I was quite happy about `3sg` until I was informed that it would 
only be done by tomorrow, as the very clever design of `3sg` caused a series of 
segfaults. Upon threatening my friend to implement `3sg` in Haskell, it was 
fixed and ready by that evening.

[^note]: and I will in the future. The shell script is easy and works fine, but
  it would be useful to have a more maintainable build system, as well as support 
  for features like tags etc., 
  Sidenotes are also a problem, for reasons that are layed out 
  [here](https://tony-zorman.com/posts/block-sidenotes.html)

Just kidding. The segfault still exists and the project is now abandoned.
So just use a shell script and pandoc.

It turns out that Pandoc supports simple templating. This includes:

- inserting YAML frontmatter variable values
- including partials
- calling other templates

This is enough for my purposes, so I went with writing a shell script that
builds this very website.

## The shell script

I needed a simple website, I knew what it should look like, and I didn't aim to
build a general purpose static site generator. Thats why the code is neither
modular nor general, and it doesn't have to be. 

We want to:

- build the landing page
- build the blog overview page
- build all the blog posts
- build the harmful subpage
- create an rss feed
- write all the generated content in the appropriate directory structure

So here we go. Straightforward and _acceptable_, but not more than that. We
definitely need to use Hakyll for that.
```bash
#!/bin/bash

TARGET_DIR="purelyf"
INCLUDE_DIR="static"
BLOGTABLE="templates/blog_table.html"
NAVBAR="$INCLUDE_DIR/navbar.html"
STYLE="$INCLUDE_DIR/style.css"
SCRIPT="$INCLUDE_DIR/script.js"

rm -rf "$TARGET_DIR"
mkdir -p "$TARGET_DIR/blog"
mkdir "$TARGET_DIR/harmful"
mkdir "$TARGET_DIR/contact"
mkdir "$TARGET_DIR/pgp"
mkdir "$TARGET_DIR/fonts"

cp "pgp/index.html" "$TARGET_DIR/pgp/index.html"
cp "$INCLUDE_DIR/404.html" "$TARGET_DIR/404.html"
cp "$INCLUDE_DIR/robots.txt" "$TARGET_DIR/"
cp "$INCLUDE_DIR/image.webp" "$TARGET_DIR/image.webp"
cp "$INCLUDE_DIR/favicon/"* "$TARGET_DIR/"
cp harmful/favicon/* "$TARGET_DIR/harmful/"
cp fonts/* "$TARGET_DIR/fonts/"

mkdir "$TARGET_DIR/$INCLUDE_DIR"
cp "$STYLE" "$TARGET_DIR/$STYLE"
cp "$SCRIPT" "$TARGET_DIR/$SCRIPT"

echo "<!-- generated using bash -->" > "$BLOGTABLE"
find blog/ -type f -name "*.md" | sort -r | while read -r file; do
    #extract the header stuff in the markdown files
    sed -n '/^---$/,/^---$/ { /^---$/d; p }' "$file" > .header_tmp

    # extract metadata
    TITLE=$(yq -r ".title" .header_tmp)
    URLTITLE=$(yq -r ".urltitle" .header_tmp)
    DATE=$(yq -r ".date" .header_tmp)
    DESCRIPTION=$(yq -r ".description" .header_tmp)

    # make blog entry
    mkdir "$TARGET_DIR/blog/$URLTITLE"

    pandoc "$file" \
            --template="templates/blogtemplate.html" \
            --filter pandoc-sidenote \
            --mathjax \
            -o "$TARGET_DIR/blog/$URLTITLE/index.html"

    echo "Built '$file'"
    
    POSTITEM=$(cat << EOF
<div class="post">
    <h1><a href="$URLTITLE">$TITLE</a></h1>
    <time>$DATE</time>
    <p align="justify">$DESCRIPTION</p>
</div>
EOF
)
    echo "$POSTITEM" >> "$BLOGTABLE"
    rm .header_tmp
done

# make index.html
pandoc "index.md" \
        --template="templates/hometemplate.html" \
        --output="$TARGET_DIR/index.html"

# make harmful/index.html
pandoc "harmful/index.md" \
        --template="templates/harmfultemplate.html" \
        --output="$TARGET_DIR/harmful/index.html"


# make blog/index.html
pandoc -i "$INCLUDE_DIR/blogindex.md" \
        --template="templates/blogindex.html" \
        --output "$TARGET_DIR/blog/index.html"


# On harmful, have a quote of the day, place it using sed and rerun each day
# this thing is not BLAZINGLY EPICLY FAST
# Want the posts to be sorted correctly? Add them YYYY-MM-DD-title.md
# No this doesn't rearrange your dependencies so you can write in a retarded
# order
```
